const adb = require('adbkit');
const { prompt } = require('enquirer');
const PngQuant = require('pngquant');
const { BufferListStream } = require('bl');
const sharp = require('sharp');
// const low = require('lowdb');
// const FileSync = require('lowdb/adapters/FileSync');

// const adapter = new FileSync('db.json');
// const db = low(adapter);

const myPngQuanter = new PngQuant(['--speed', '10', '--quality', '1', '-']);

const client = adb.createClient();

async function main() {
  const devices = await client.listDevices();
  const question = {
    type: 'select',
    name: 'deviceId',
    message: 'Select device?',
    choices: devices.map((device) => ({
      name: device.id,
      message: device.id,
      value: device.id,
    })),
  };
  const { deviceId } = await prompt(question);

  const imageBuffer = BufferListStream((err, data) => {
    sharp(data)
      .extract({
        width: 200,
        height: 200,
        left: 0,
        top: 0,
      })
      .toFile('./buffered.png');
  });
  client.screencap(deviceId, (err, screencap) => {
    screencap.pipe(myPngQuanter).pipe(imageBuffer);
  });
}
main();
